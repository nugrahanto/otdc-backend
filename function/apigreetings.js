var express = require('express');
var router  = express.Router();
var db 		= require('./dbconnection');
var auth 	= require('./jwt.js');

//add pasien data
router.post('/add/', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("insert into greetings(greetings) values(?)",
			[req.body.greetings],function(err,results){
			if(err)
			{
				res.json(err);
			}
			else
			{
				res.status(200).send("Ucapan baru berhasil ditambahkan");
			}
		});
	});
});

//update pasien data by pasien ID, need parameter pasien ID
router.put('/edit/:param', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("select greetings_id from greetings where greetings_id=?",[req.params.param],function(err,rows){
			if(rows.length > 0)
			{
				connection.query("update greetings set greetings=? where greetings_id=?",
				[req.body.greetings,req.params.param],function(err,results){
					if(err)
					{
						res.json(err)
					}
					else
					{
						res.status(201).send("Data ucapan berhasil dirubah");
					}
				});
			}
			else
			{
				res.status(400).send("Id Ucapan tidak ditemukan!");
			}
		});
	});
});

//delete pasien data by pasien ID, need parameter pasien ID
router.delete('/delete/:param', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("delete from greetings where greetings_id=?",[req.params.param],function(err,results){
			if(results.affectedRows != 0)
			{
				res.status(200).send("Data ucapan berhasil dihapus");
			}
			else
			{
				res.status(400).send("Id ucapan tidak ditemukan");
			}
		});
	});
});

//get All pasien list
router.get('/all/', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("select * from greetings",function(err, rows, fields){
			if(err)
			{
				res.json(err)
			}
			else
			{
				res.json(rows);
			}
		});
	});
});

//get pasien list by pasien ID, need parameter pasien ID
router.get('/data/:param', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("select * from greetings where greetings_id=?",[req.params.param],function(err, rows, fields){
			if(err)
			{
				res.json(err)
			}
			else
			{
				res.json(rows);
			}
		});
	});
});

module.exports = router;
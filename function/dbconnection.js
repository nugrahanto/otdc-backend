var mysql = require('mysql');
var connection = mysql.createPool({

	host:'localhost',
	user:'root',
	password:'',
	database:'otdcdb'

});

// db test connection
// uncomment following the line for test ur database connection,
// comment the line after you use it. 
/*
connection.query('SELECT * FROM user', (error, results, fields) => {
  if (error) throw error;
  console.log('All data: ', results);
});
*/
module.exports = connection;
var express = require('express');
var router  = express.Router();
var db 		= require('./dbconnection');
var auth 	= require('./jwt.js');

//get All pasien list
router.get('/detail/', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("SELECT * FROM bdhistory ORDER BY create_date DESC",
			function(err, rows, fields){
			if(err)
			{
				res.json(err)
			}
			else
			{
				res.json(rows);
			}
		});
	});
});

module.exports = router;
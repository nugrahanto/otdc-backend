var jwt = require('jsonwebtoken');

module.exports = {
	'secret': 'gridOTDC',

	verifyToken: function(req, res, next){
		var token = req.headers['authorization'];

		if(!token)
		{
			return res.status(401).send({ auth: false, message: 'Gagal mengakses data, token tidak ditemukan' });
		}
		else
		{
			jwt.verify(token, module.exports.secret, function(err, decoded) {
				if(err)
				{
					return res.status(403).send({ auth: false, message: 'Otentikasi token gagal' });
				}
				else
				{
					req.userId 		 = decoded.id;
					req.userUsername = decoded.username;
					req.userFName 	 = decoded.firstname;
					req.userLName 	 = decoded.lastname;
					next();
				}
			});
		}
	}
};
var express      = require('express');
var path         = require('path');
var cors         = require('cors');
var cron         = require('node-cron');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var userApi      = require('./apiuser');
var pasienApi    = require('./apipasien');
var greetingsApi = require('./apigreetings');
var notifApi     = require('./apinotification');
var dbhistory    = require('./crondbhistory');

var app          = express();

/* view engine setup */
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'pug');

/* environment setup */
app.set('env', 'production');

/* cors setup */
app.use(cors());

/* body parser setup*/
// parse application/json
app.use(bodyParser.json({limit: '50mb'}));
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({limit: '50mb', extended: false }));

/* cookie parser setup*/
app.use(cookieParser());

/* GET API home page. */
routes = express.Router().get('/', function(req, res, next) {
  res.render('index', { title: 'goodluck' });
});

/* Router set */
app.use('/', routes);
app.use('/user', userApi);
app.use('/pasien', pasienApi);
app.use('/greetings', greetingsApi);
app.use('/notification', notifApi);

/* node cron */
cron.schedule("1 00 00 * * *", function() {
  dbhistory.run();
});

/* error handlers */

// error 404
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
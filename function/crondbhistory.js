var db = require('./dbconnection');

var dbhistory = {
	run: function(){
		console.log('cron start');
		db.getConnection(function(err,connection){
			connection.query("SELECT *, DATE_FORMAT(born_date,\'%Y-%m-%d\') as born_date, YEAR(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\')) - YEAR(born_date) as age FROM pasien WHERE (MONTH(born_date)=MONTH(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\')) AND DAY(born_date)=DAY(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\'))) OR (DAY(LAST_DAY(born_date))=29 AND DAY(born_date)=29 AND DAY(LAST_DAY(DATE_FORMAT(CURDATE(),\'%Y-%m-%d\')))=28)",
				function(err, rows, fields){
				if(rows.length > 0)
				{
					//console.log(rows);
					for (var i = 0; i < rows.length; i++) {
						connection.query("insert into bdhistory(firstname,lastname,address,born_date,age,gender,contact) values(?,?,?,?,?,?,?)",
						[rows[i].firstname,rows[i].lastname,rows[i].address,rows[i].born_date,rows[i].age,rows[i].gender,rows[i].contact],function(err,results){
							if(err) 
							{
								console.log(err);
							}
						});
					}
				}
			});
		});
	}
};

module.exports = dbhistory;
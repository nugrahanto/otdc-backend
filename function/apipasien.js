var express = require('express');
var router  = express.Router();
var db 		= require('./dbconnection');
var auth 	= require('./jwt.js');

//add pasien data
router.post('/add/', auth.verifyToken, function(req,res,next){
	if (!req.body.contact) 
	{
		return res.status(400).send("Mohon masukkan Nomer HP pasien!");
	} 
	else 
	{
		db.getConnection(function(err,connection){
			connection.query("select contact from pasien where contact=?",[req.body.contact],function(err,rows){
				if(rows.length == 0)
				{
					connection.query("insert into pasien(contact,firstname,lastname,address,gender,born_place,born_date,treatment) values(?,?,?,?,?,?,?,?)",
						[req.body.contact,req.body.firstname,req.body.lastname,req.body.address,req.body.gender,req.body.born_place,req.body.born_date,req.body.treatment],function(err,results){
						if(err)
						{
							res.json(err);
						}
						else
						{
							res.status(200).send("Data pasien berhasil ditambahkan");
						}
					});
				}
				else
				{
					res.status(400).send("Nomer HP sudah terdaftar!");
				}
			});
		});
	}
});

//update pasien data by pasien ID, need parameter pasien ID
router.put('/edit/:param', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("select pasien_id from pasien where pasien_id=?",[req.params.param],function(err,rows){
			if(rows.length > 0)
			{
				connection.query("update pasien set contact=?, firstname=?, lastname=?, address=?, gender=?, born_place=?, born_date=?, treatment=? where pasien_id=?",
				[req.body.contact,req.body.firstname,req.body.lastname,req.body.address,req.body.gender,req.body.born_place,req.body.born_date,req.body.treatment,req.params.param],function(err,results){
					if(err)
					{
						res.json(err)
					}
					else
					{
						res.status(201).send("Data pasien berhasil dirubah");
					}
				});
			}
			else
			{
				res.status(400).send("Nomer HP tidak ditemukan!");
			}
		});
	});
});

//delete pasien data by pasien ID, need parameter pasien ID
router.delete('/delete/:param', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("delete from pasien where pasien_id=?",[req.params.param],function(err,results){
			if(results.affectedRows != 0)
			{
				res.status(200).send("Data pasien berhasil dihapus");
			}
			else
			{
				res.status(400).send("Id pasien tidak ditemukan");
			}
		});
	});
});

//get All pasien list
router.get('/all/', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("select *, DATE_FORMAT(born_date,\'%Y-%m-%d\') as born_date from pasien",function(err, rows, fields){
			if(err)
			{
				res.json(err)
			}
			else
			{
				res.json(rows);
			}
		});
	});
});

//get pasien list by pasien ID, need parameter pasien ID
router.get('/data/:param', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("select *, DATE_FORMAT(born_date,\'%Y-%m-%d\') as born_date  from pasien where pasien_id=?",[req.params.param],function(err, rows, fields){
			if(err)
			{
				res.json(err)
			}
			else
			{
				res.json(rows);
			}
		});
	});
});

module.exports = router;
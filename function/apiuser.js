var express = require('express');
var jwt 	= require('jsonwebtoken');
var bcrypt 	= require('bcrypt');
var router 	= express.Router();
var db 		= require('./dbconnection');
var auth 	= require('./jwt.js');

function createToken(user) {
	return jwt.sign(user, auth.secret, { expiresIn: 60*60*5 });
};

//login
router.post('/signin/',function(req,res,next){
	if (!req.body.username || !req.body.password) 
	{
		return res.status(400).send("Mohon masukkan username dan password!");
	} 
	else 
	{
		db.query("select * from user where username=?",[req.body.username],function(err,rows){
			if(rows.length > 0)
			{
				var passwordIsValid = bcrypt.compareSync(req.body.password, rows[0].password);
				if(!passwordIsValid)
				{
					res.status(400).send("Password yang anda masukkan salah!");
				}
				else
				{
					User = {
						id: rows[0].user_id,
						username: rows[0].username,
						firstname: rows[0].firstname,
						lastname: rows[0].lastname
					};

					res.status(200).send({
						id_token: createToken(User)
					});
				}
			}
			else
			{
				res.status(400).send("Username tidak terdaftar!");
			}
		});
	}
});

//create user
router.post('/signup/',function(req,res,next){
	if (!req.body.username || !req.body.password) {
		return res.status(400).send("Mohon masukkan username dan password!");
	} else {
		db.getConnection(function(err,connection){
			connection.query("select * from user where username=?",[req.body.username],function(err,rows){
				if(rows.length > 0)
				{
					res.status(400).send("Username telah digunakan");
				}
				else
				{
					var hashedPassword = bcrypt.hashSync(req.body.password, 8);

					connection.query("insert into user(username,password,firstname,lastname) values(?,?,?,?)",
					[req.body.username,hashedPassword,req.body.firstname,req.body.lastname],function(err,results){
						if(err)
						{
							res.json(err);
						}
						else
						{
							newUser = {
								id: results.insertId,
								username: req.body.username,
								firstname: req.body.firstname,
								lastname: req.body.lastname
							};

							res.status(200).send({
								id_token: createToken(newUser)
							});
						}
					});
				}
			});
		});
	}
});

//update user by ID or username, need parameter id or username user
router.put('/edit/:param', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("select * from user where user_id=?",[req.params.param],function(err,rows){
			if(rows.length > 0)
			{
				var hashedPassword = bcrypt.hashSync(req.body.password, 8);
				connection.query("update user set username=?, password=?, firstname=?, lastname=? where user_id=?",
				[req.body.username,hashedPassword,req.body.firstname,req.body.lastname,req.params.param],function(err,results){
					if(err)
					{
						res.json(err)
					}
					else
					{
						res.status(201).send("Data user berhasil dirubah");
					}
				});
			}
			else
			{
				connection.query("select * from user where username=?",[req.params.param],function(err,rows){
					if(rows.length > 0)
					{
						var hashedPassword = bcrypt.hashSync(req.body.password, 8);
						connection.query("update user set username=?, password=?, firstname=?, lastname=? where username=?",
						[req.body.username,hashedPassword,req.body.firstname,req.body.lastname,req.params.param],function(err,results){
							if(err)
							{
								res.json(err)
							}
							else
							{
								res.status(201).send("Data user berhasil dirubah");
							}
						});
					}
					else
					{
						res.status(400).send("id ataupun username tidak ditemukan");
					}
				});
			}
		});
	});
});

//delete user by ID or username, need parameter user id or username
router.delete('/delete/:param', auth.verifyToken, function(req,res,next){
	db.query("delete from user where user_id=?",[req.params.param],function(err,results){
		if(results.affectedRows != 0)
		{
			res.status(200).send("Data user berhasil dihapus");
		}
		else
		{
			db.query("delete from user where username=?",[req.params.param],function(err,results){
				if(results.affectedRows != 0)
				{
					res.status(200).send("Data user berhasil dihapus");
				}
				else
				{
					res.status(400).send("id ataupun username tidak ditemukan");
				}
			});
		}
	});
});

//get All user list
router.get('/all/', auth.verifyToken, function(req,res,next){
	db.query("select * from user",function(err, rows, fields){
		if(err)
		{
			res.json(err)
		}
		else
		{
			res.json(rows);
		}
	});
});

//get user list by id or username, need parameter id or username
router.get('/data/:param?', auth.verifyToken, function(req,res,next){
	db.query("select * from user where user_id=?",[req.params.param],function(err,rows){
		if(rows.length > 0)
		{
			res.json(rows);
		}
		else 
		{
			db.query("select * from user where username=?",[req.params.param],function(err,rows){
				if(err)
				{
					res.json(err)
				}
				else
				{
					res.json(rows);
				}
			});
		}
	});
});

module.exports = router;